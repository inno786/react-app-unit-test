const services = require('../services');

module.exports.actions = (req, res, next) => {
    services[req.headers.service][req.headers.method](req, res, next);
};