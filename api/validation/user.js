const BaseJoi = require('joi');
const dateFormat = require('dateformat');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

/**
 * Starts: Validation schema
 */
let schemaUserCreate = Joi.object().keys({
    fname: Joi.string().min(3).max(15).required().trim().regex(/^[\w\-\s]+$/)
        .options({
            language: {
                string: {
                    regex: {
                        base: 'only alphabets are allowed.'
                    }
                }
            }
        }).label('First Name'),
    lname: Joi.string().min(3).max(15).required().trim().regex(/^[\w\-\s]+$/)
        .options({
            language: {
                string: {
                    regex: {
                        base: 'only alphabets are allowed.'
                    }
                }
            }
        }).label('Last Name'),
    email: Joi.string().email().required().label('Email Address'),
    password: Joi.string().min(6).max(15).required().trim().label('Password'),
    dob: Joi.date().min(dateFormat(new Date(), "yyyy-mm-dd")).required(),
    designation: Joi.string().max(32).required().trim().label('Designation'),
    code: Joi.string().max(32).required().trim().label('Code'),
    status: Joi.number().valid(0, 1).required().label('Status')
});
// Login
let schemaUserLogin = Joi.object().keys({
    email: Joi.string().email().required().label('Email Address'),
    password: Joi.string().min(6).max(15).required().trim().label('Password')
});

const validateUserCreate = input => {
    return new Promise(resolve => {
        Joi.validate(input, schemaUserCreate, {
            abortEarly: false,
            language: {},
            escapeHtml: true,
            noDefaults: true
        }, (error, value) => {
            if (error) return resolve({
                error: error.details.map(item =>
                    ({ key: item.path[0], value: item.message.replace(new RegExp('"', 'g'), '') })
                )
            });
            return resolve({ error: null, value });
        });
    });
}
const validateLogin = input => {
    return new Promise(resolve => {
        Joi.validate(input, schemaUserLogin, {
            abortEarly: false,
            language: {},
            escapeHtml: true,
            noDefaults: true
        }, (error, value) => {
            if (error) return resolve({
                error: error.details.map(item =>
                    ({ key: item.path[0], value: item.message.replace(new RegExp('"', 'g'), '') })
                )
            });
            return resolve({ error: null, value });
        });
    });
}

module.exports = {
    validateUserCreate,
    validateLogin
}