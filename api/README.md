## This is a complete backend CRUD API.

## Dependency
1. Mongo >=4
2. Nodejs >=9

## How to setup application?

1. Install mongodb
## Linux:
 https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

## Windows:
1. Download setup; https://www.mongodb.com/download-center/community
2. Install setup
3. Make a directory in any drive. Like, D:\data\db
4. Now, open terminal and go to C:\Program Files\MongoDB\Server\3.2\bin> and type 
mongod.exe --dbpath="D:\data\db"
Press Enter key. After pressing Enter key, a few minute latter you will see connection stablished message.

## How to run application?
1. Start mongo db
2. npm install (one time)
3. npm start


