require('dotenv').config();
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();

require('./models').connect('mongodb://localhost/curd');

// Apply middlyware for user input
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

const corsOptions = {
    origin: process.env.WHITE_LIST,
    optionsSuccessStatus: 200
}
// Apply routes
let route = require('./routers')
app.use('/', cors(corsOptions), route);

// Start server
app.listen(process.env.PORT, () => {
    console.log(`Server is running port* ${process.env.PORT}`)
})