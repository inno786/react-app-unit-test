const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    fname: {
        type: String,
        required: true,
        min: 3,
        max: 15,
    },
    lname: {
        type: String,
        required: true,
        min: 3,
        max: 15,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        max: 150
    },
    password: {
        type: String,
        required: true,
        max: 150,
    },
    dob: {
        type: Date, default: Date.now()
    },
    designation: {
        type: String,
        required: true,
        max: 150
    },
    code: {
        type: String,
        max: 32,
        defaultValue: ''
    },
    status: {
        type: Number, default: 0
    },
    createdAt: {
        type: Date, default: Date.now()
    },
    updatedAt: {
        type: Date, default: Date.now()
    }
});

module.exports = mongoose.model('User', UserSchema);