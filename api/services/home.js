
let index = (req, res, next) => {
    return res.status(200).json({ route: "home" });
};

module.exports = { index };