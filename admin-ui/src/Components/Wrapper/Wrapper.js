import React from 'react';
import WrapperStyle from './WrapperStyle';

const Wrapper = props => (
    <div style={WrapperStyle.container} id={props.id}>
        {props.children}
    </div>
);

export default Wrapper;