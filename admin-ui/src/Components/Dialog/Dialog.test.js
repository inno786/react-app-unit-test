import React from 'react';

import { ShallowRender } from '../../Services/Utils/ShallowRender';
import Dialog from './Dialog';
// import { Modal } from 'react-bootstrap';

describe('<Dialog />', () => {
    let enzymeWrapper;
    let props;
    beforeAll(() => {
        props = {
            title: "Delete user record",
            content: "Are you sure?",
            isOpen: true,
            onSubmit: jest.fn(),
            onCancel: jest.fn()
        }
        enzymeWrapper = ShallowRender(<Dialog {...props} />);
    });
    it('should render a section without errors', () => {
        expect(enzymeWrapper).toMatchSnapshot();
    });
});
