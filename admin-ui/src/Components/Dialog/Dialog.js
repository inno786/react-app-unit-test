import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-bootstrap';
import CustomButton from '../../Components/Button/CustomButton';
import { bodyContent, footerContent, onSubmit, onCancel } from './DialogStyles';

class Dialog extends React.Component {

    constructor(props) {
        super(props);

        this.onConfirm = this.onConfirm.bind(this);
        this.onClose = this.onClose.bind(this);
    }
    onConfirm() {
        this.props.onSubmit();
    }
    onClose() {
        this.props.onCancel();
    }

    render() {
        return (
            <Modal
                show={this.props.isOpen}
                onHide={this.onClose}
                animation={false}
            >
                <Modal.Header>
                    {this.props.title}
                </Modal.Header>
                <Modal.Body>
                    <div className={bodyContent}>
                        {this.props.content}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <div className={footerContent}>
                        <div className={onSubmit}>
                            <CustomButton displayText="Confirm" clickHandler={this.onConfirm} />
                        </div>
                        <div className={onCancel}>
                            <CustomButton displayText="Cancel" clickHandler={this.onClose} />
                        </div>
                    </div>
                </Modal.Footer>
            </Modal>
        );
    }
}

Dialog.defaultProps = {
    title: '',
    content: '',
    onSubmit: () => { },
    onCancel: () => { }
};

Dialog.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    isOpen: PropTypes.bool,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

export default Dialog;
