import { css } from 'emotion';

export const bodyContent = css`
  padding: 10px;
  text-align: left;
`;
export const footerContent = css`
  padding: 10px;
  text-align: center;
  width: 50%;
`;
export const onSubmit = css`
  float: left;
`;
export const onCancel = css`
  float: left;
`;