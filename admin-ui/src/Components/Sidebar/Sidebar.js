import React from 'react';
import { Link } from 'react-router-dom';

const Sidebar = () => (
    <div>
        <Link to="/user/list">User List</Link> <br />
        <Link to="/user/create">New User</Link>
    </div>
);

export default Sidebar;