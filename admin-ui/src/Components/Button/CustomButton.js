import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import { btnPadding } from './ButtonStyle';

const CustomButton = ({ size, type, variant, customClass, bsStyle, displayText, clickHandler }) => (
    <React.Fragment>
        <Button
            size={size}
            type={type}
            variant={variant}
            className={`${btnPadding} ${customClass}`}
            bsStyle={bsStyle}
            onClick={() => clickHandler()}
        >
            {displayText}
        </Button>
    </React.Fragment>
);

CustomButton.defaultProps = {
    size: 'sm',
    type: 'button',
    variant: 'primary',
    customClass: '',
    bsStyle: 'info',
    displayText: 'Button',
    clickHandler: () => { }
};

CustomButton.propTypes = {
    size: PropTypes.string,
    type: PropTypes.string,
    variant: PropTypes.string,
    customClass: PropTypes.string,
    bsStyle: PropTypes.string,
    displayText: PropTypes.string,
    clickHandler: PropTypes.func,
};

export default CustomButton;