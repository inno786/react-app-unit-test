import React from 'react';

import { ShallowRender } from '../../Services/Utils/ShallowRender';
import CustomButton from './CustomButton';
import { Button } from 'react-bootstrap';

describe('<CustomButton />', () => {
    let enzymeWrapper;
    let props;
    beforeAll(() => {
        props = {
            size: 'sm',
            type: 'button',
            variant: 'primary',
            customClass: '',
            bsStyle: 'info',
            displayText: 'Login',
            clickHandler: jest.fn()
        }
        enzymeWrapper = ShallowRender(<CustomButton {...props} />);
    });
    it('should render a section without errors', () => {
        expect(enzymeWrapper).toMatchSnapshot();
    });
    it('should click on button', () => {
        enzymeWrapper.find(Button).props().onClick();
        expect(props.clickHandler).toHaveBeenCalled();
    });
});
