
const styles = {
    table: {
        width: '100%'
    },
    header: {
        width: '100%',
        padding: 20,
        textAlign: 'center'
    },
    sidebar: {
        width: '15%',
        padding: 20,
        textAlign: 'center'
    },
    center: {
        width: '85%',
        padding: 20,
    },
    footer: {
        width: '100%',
        padding: 20,
        textAlign: 'center'
    },
    title: {
        textAlign: 'left'
    }
}

export default styles;