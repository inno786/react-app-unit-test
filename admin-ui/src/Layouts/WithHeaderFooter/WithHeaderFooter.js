import React from 'react';
import PropTypes from 'prop-types';
import { ToastContainer } from 'react-toastify';

import Wrapper from '../../Components/Wrapper/Wrapper';
import Sidebar from '../../Components/Sidebar/Sidebar';
import styles from './WithHeaderFooterStyle';

const WithHeaderFooter = props => (
    <Wrapper id={2}>
        <ToastContainer />
        <table style={styles.table} border={1} align="center">
            <tbody>
                <tr style={styles.header}>
                    <td colSpan={2}>Header</td>
                </tr>
                <tr>
                    <td style={styles.sidebar}>
                        <Sidebar />
                    </td>
                    <td style={styles.center}>
                        <h4 style={styles.title}>{props.title}</h4>
                        {props.bodyContent}
                    </td>
                </tr>
                <tr style={styles.footer}>
                    <td colSpan={2}>Footer</td>
                </tr>
            </tbody>
        </table>
        {props.footerContent}
    </Wrapper>
)
WithHeaderFooter.defaultProps = {
    title: '',
    footerContent: ''
};

WithHeaderFooter.propTypes = {
    title: PropTypes.string.isRequired,
    bodyContent: PropTypes.any,
    footerContent: PropTypes.any
}
export default WithHeaderFooter;