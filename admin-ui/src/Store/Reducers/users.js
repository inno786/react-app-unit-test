import { USER_LIST} from '../Constants/ActionTypes';

const initilize = {
    designation: [
        { id: 1, label: 'CEO', value: 'CEO' },
        { id: 2, label: 'ReactJs Developer', value: 'ReactJs Developer' },
        { id: 3, label: 'Angular Developer', value: 'Angular Developer' },
    ]
};

const users = (state = initilize, action) => {
    switch (action.type) {
        case USER_LIST:
            return {
                ...state
            }
        default:
            return {
                ...state
            }
    }
};

export default users;