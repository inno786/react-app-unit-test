import * as types from '../Constants/ActionTypes';

export const onDelete = id => dispatch => {
    console.log('delete action =>', id);
    dispatch({
        type: types.ON_DELETE,
        payload: { id }
    })
}

export const onAdd = data => dispatch => {
    console.log('Add action =>', data);
    dispatch({
        type: types.ON_ADD,
        payload: { data }
    })
}
export const onUpdate = payload => dispatch => {
    console.log('Update action =>', payload);
    dispatch({
        type: types.ON_UPDATE,
        payload
    })
}