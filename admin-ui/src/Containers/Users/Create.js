import React, { Component } from 'react';
import { connect } from 'react-redux';
import WithHeaderFooter from '../../Layouts/WithHeaderFooter/WithHeaderFooter';
import UserForm from './Components/UserForm';
import './Components/User.css';
import ApiUser from '../../Services/Api/User';
import { errorHandler, Notifications } from '../../Services/Utils';
const api = new ApiUser();

class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            formData: {
                fname: '',
                lname: '',
                email: '',
                password: '',
                dob: '',
                designation: '',
                status: 1
            },
            errors: {
                fname: '',
                lname: '',
                email: '',
                password: '',
                dob: '',
                designation: '',
                status: ''
            }
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(event) {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }
    // call on Delete event.
    async onSubmit(event) {
        event.preventDefault();
        const { formData, errors } = this.state;
        const result = await api.create(formData);
        const { data } = result.response;
        if (data.error) {
            if (data.message) Notifications('error', data.message);
            const error = errorHandler(errors, data.error);
            console.log('error =>', data.error);
            this.setState({ errors: error, message: data.message ? data.message : '' });
        } else {
            this.props.history.push('/user/list');
        }
    }

    render() {
        const { data } = this.props;
        return (
            <WithHeaderFooter
                {...this.props}
                title="Create New User"
                bodyContent={
                    <table className="user-container">
                        <tbody>
                            <tr>
                                <td>
                                    <UserForm
                                        {...this.state}
                                        designation={data}
                                        onChange={this.onChange}
                                        onSubmit={this.onSubmit}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                }
            />
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.users.designation
    }
}

const Container = connect(mapStateToProps, null)(Create);
export default Container;