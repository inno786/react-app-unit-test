import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import WithHeaderFooter from '../../Layouts/WithHeaderFooter/WithHeaderFooter';
import ListItem from './Components/ListItem';
import CustomButton from '../../Components/Button/CustomButton';
import Dialog from '../../Components/Dialog/Dialog';
import ApiUser from '../../Services/Api/User';
import './Components/User.css';
const api = new ApiUser();

class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            modalBoxStatus: false,
            title: 'Delete user record',
            content: 'Are you sure?',
            users: []
        };
        this.showModalBox = this.showModalBox.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onClose = this.onClose.bind(this);
    }
    async componentDidMount() {
        const result = await api.list();
        const { data } = result.response.data;
        this.setState({ users: data });
    }
    showModalBox(id) {
        this.setState({ id, modalBoxStatus: true });
    }
    // call on Delete event.
    async onDelete() {
        const { id } = this.state;
        const result = await api.remove(id);
        const { data } = result.response.data;
        if(data.error) {
            this.setState({ content: 'Error occured.'});
        } else {
            this.setState({ users: data, id: '', modalBoxStatus: false});
        }
    }
    onClose() {
        this.setState({ id: '', modalBoxStatus: false });
    }

    render() {
        const { users, title, content, modalBoxStatus } = this.state;
        return (
            <WithHeaderFooter
                {...this.props}
                title="User List"
                bodyContent={
                    <table border={1} className="user-container">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>DOB</th>
                                <th>Designation</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.length > 0 && users.map((row, idx) => (
                                <ListItem
                                    item={row}
                                    onDelete={this.showModalBox}
                                    key={idx}
                                />
                            ))}
                            <tr>
                                <td colSpan="8">
                                    <Link to="/user/create"><CustomButton size="lg" type="button" displayText="Create New User" /></Link>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                }
                footerContent={(<Dialog
                    title={title}
                    content={content}
                    isOpen={modalBoxStatus}
                    onSubmit={this.onDelete}
                    onCancel={this.onClose}
                />)}
            />
        )
    }
}

export default List;