const style = {
    error: {
        color: 'red',
        fontSize: 12
    },
    divStyle: {
        alignItems: 'center',
    },
    panelStyle: {
        backgroundColor: 'rgba(255,255,255,0.5)',
        border: 0,
        paddingLeft: 20,
        paddingRight: 20,
        width: 500,
    },
    buttonStyle: {
        marginBottom: 0
    }
}

export default style;