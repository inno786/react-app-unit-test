import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment'
import WithHeaderFooter from '../../Layouts/WithHeaderFooter/WithHeaderFooter';
import UserForm from './Components/UserForm';
import './Components/User.css';
import ApiUser from '../../Services/Api/User';
import { errorHandler, Notifications } from '../../Services/Utils';
const api = new ApiUser();

class Update extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            id: '',
            formData: {
                fname: '',
                lname: '',
                email: '',
                password: '',
                dob: '',
                designation: '',
                status: 1
            },
            errors: {
                fname: '',
                lname: '',
                email: '',
                password: '',
                dob: '',
                designation: '',
                status: ''
            }
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    async componentDidMount() {
        const { id } = this.props.match.params;
        if (id) {
            const result = await api.findUserById(id);
            if (result.error) {

            } else {
                const { data } = result.response.data;
                data.dob = moment(data.dob, 'YYYY-MM-DD').isValid() ? data.dob : '';
                this.setState({ formData: data, id });
            }
        } else {

        }
    }
    onChange(event) {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }
    async onSubmit(event) {
        event.preventDefault();
        const { id, formData, errors } = this.state;
        const result = await api.update(id, formData);
        const { data } = result.response;
        if (data.error) {
            if (data.message) Notifications('error', data.message);
            const error = errorHandler(errors, data.error);
            console.log('error =>', data.error);
            this.setState({ errors: error, message: data.message ? data.message : '' });
        } else {
            this.props.history.push('/user/list');
        }
    }

    render() {
        const { data } = this.props;
        return (
            <WithHeaderFooter
                {...this.props}
                title="Update User"
                bodyContent={
                    <table className="user-container">
                        <tbody>
                            <tr>
                                <td>
                                    <UserForm
                                        {...this.state}
                                        designation={data}
                                        onChange={this.onChange}
                                        onSubmit={this.onSubmit}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                }
            />
        )
    }
}
const mapStateToProps = state => {
    return {
        data: state.users.designation
    }
}

const Container = connect(mapStateToProps, null)(Update);
export default Container;