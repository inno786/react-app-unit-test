import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <div align="center">
                <h1>Home Page</h1>
                <Link to="/login">Go to login</Link> | <Link to="/user/create">Create New User</Link>
            </div>

        )
    }
}

export default Home;