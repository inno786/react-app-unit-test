import React, { Component } from 'react';

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        };
        this.increaseCounter = this.increaseCounter.bind(this);
    }
    componentWillMount() {
        console.log('call componentWillMount');
    }
    componentDidMount() {
        console.log('call componentDidMount');
    }
    shouldComponentUpdate(newProps, newState) {
        console.log('call shouldComponentUpdate');
        return true;
    }
    componentWillUpdate(newProps, newState) {
        console.log('call componentWillUpdate');
    }
    componentDidUpdate(prevProps) {
        console.log('call componentDidUpdate');
    }
    increaseCounter() {
        console.log('click me call');
        this.setState(prevState => {
            return {
                count: prevState.count + 1
            }
        }, () => {
            console.log('state has been updated');
        })
    }
    render() {
        console.log('call render');
        return (
            <div align="center">
                <h1>Life cycle</h1>
                <div onClick={this.increaseCounter}>Click Me</div>
                <p>{`Counter: ${this.state.count}`}</p>
            </div>
        )
    }
}

export default Counter;