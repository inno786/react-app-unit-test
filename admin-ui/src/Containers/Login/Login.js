import React, { Component } from 'react';
import LoginForm from './Components/LoginForm';
import './Components/Login.css';
import ApiLogin from '../../Services/Api/Login';
import { errorHandler, Notifications } from '../../Services/Utils';
import { ToastContainer } from 'react-toastify';

const api = new ApiLogin();

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            formData: {
                email: '',
                password: ''
            },
            errors: {
                email: '',
                password: ''
            }
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(event) {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }

    async onSubmit(event) {
        event.preventDefault();

        const { formData, errors } = this.state;
        const result = await api.login(formData);
        const { data } = result.response;

        if (data.error) {
            if (data.message) Notifications('error', data.message);
            const error = errorHandler(errors, data.error);
            this.setState({ errors: error, message: data.message ? data.message : '' });
        } else {
            if (data.message) Notifications('success', data.message);
            this.props.history.push('/user/list');
        }
    }
    render() {
        return (
            <div align="center" className="Login">
                <ToastContainer />
                <LoginForm
                    {...this.state}
                    onChange={this.onChange}
                    onSubmit={this.onSubmit}
                />
            </div>
        )
    }
}

export default Login;