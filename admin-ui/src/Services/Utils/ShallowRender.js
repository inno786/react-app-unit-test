import 'dotenv/config';
import Enzyme, { mount, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

jest.setTimeout(process.env.REACT_APP_JEST_TIMEOUT);
Enzyme.configure({ adapter: new Adapter() });

export const ShallowRender = Node => shallow(Node);
export const MountRender = Node => mount(Node);
