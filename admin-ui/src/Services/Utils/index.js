import { toast } from 'react-toastify';
import { css } from 'emotion';

export const errorHandler = (errors, response) => {
    Object.keys(errors).map(key => {
        const obj = response.find(item => key === item.key); 
        if(obj) {
            errors[key] = obj.value;
        } else {
            errors[key] = '';
        }
        return errors;
    });
    return errors;
}

export const Notifications = (type, text) => toast[type](text, {
    position: toast.POSITION.TOP_CENTER,
    className: css({
        minHeight: 45
      })
})