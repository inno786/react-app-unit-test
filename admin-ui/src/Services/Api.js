import axios from 'axios';

/**
 * API service.
 *
 * @class
 * @copyright 2018 All Rights Reserved.
 */
class Api {
    /**
     * Create an instance of Api.
     *
     * @memberof Api
     */
    constructor() {
        this.instance = axios.create({
            baseURL: process.env.REACT_APP_API_URL,
        });
        this.params = { jwtToken: '' };
    }

    /**
     * Wrapper for GET request.
     *
     * @param {string} method  The name of the API method in URL readable format.
     * @param {?object} params  The params to call the method.
     * @returns {AxiosInstance}
     * @memberof Api
     */
    get(header, params = null) {
        return this.request('get', header, params);
    }

    /**
     * Wrapper for POST request.
     *
     * @param {string} method  The name of the API method in URL readable format.
     * @param {?object} params  The params to call the method.
     * @returns {AxiosInstance}
     * @memberof Api
     */
    post(header, params = null) {
        return this.request('post', header, params);
    }

    /**
     * Store token in session to allow its reuse.
     *
     * @param {object} response  The HTTP response from a request
     * @returns {object}  The original response
     */
    setToken(response) {

    }

    /**
     * Set the authorization header with the token (if any)
     *
     * @returns {void}
     */
    setAuthHeader() {
        // Add this to the header for the HTTP request
        this.instance.defaults.headers.common.Authorization = this.params.jwtToken;
    }
    setHeader(data) {
        // Add this to the header for the HTTP request
        this.instance.defaults.headers = data;
    }

    /**
     * Main wrapper for Axios.
     *
     * @param {string} type  The request type (GET/POST).
     * @param {string|null} stub   optional stub
     * @param {string} method  The method from API to be executed.
     * @param {?object} params  The params to pass to method.
     * @returns {AxiosInstance}
     * @memberof Api
     */
    request(type, header, params = null) {
        return new Promise(resolve => {
            let instance;
            this.setHeader(header);

            if (type === 'get') {
                const query = Object.keys(params).map(key => `${key}=${params[key]}`).join('&');
                instance = this.instance.get(`/api?${query}`);
            } else {
                instance = this.instance.post('/api/', { ...params });
            }
            return instance
                .then(response => resolve({ error: null, response }))
                .catch(error => resolve({ error }));
        });
    }
}

export default Api;

