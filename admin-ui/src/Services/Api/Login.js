import Api from '../Api';

/**
 * API layer for Tests.
 *
 * @class
 * @copyright  2018 MedSims from WebMD/Medscape.  All Rights Reserved.
 * @extends Api
 */
class ApiLogin extends Api {
    /**
     * Retrieve the list of tests that have been ordered for a patient.
     *
     * @returns {object[]}
     * @memberof ApiTests
     */
    async login(data) {
        const response = await this.post({ service: 'user', method: 'login' }, data);
        return response;
    }
}

export default ApiLogin;
