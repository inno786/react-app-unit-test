import Api from '../Api';

/**
 * API layer for Tests.
 *
 * @class
 * @copyright  2018 MedSims from WebMD/Medscape.  All Rights Reserved.
 * @extends Api
 */
class ApiUser extends Api {
    /**
     * Retrieve the list of tests that have been ordered for a patient.
     *
     * @returns {object[]}
     * @memberof ApiTests
     */
    async create(data) {
        const response = await this.post({ service: 'user', method: 'create' }, data);
        return response;
    }
    async update(id, data) {
        const response = await this.post({ service: 'user', method: 'update', id }, data);
        return response;
    }
    async remove(_id) {
        const response = await this.post({ service: 'user', method: 'remove', _id }, {});
        return response;
    }
    async list() {
        const response = await this.post({ service: 'user', method: 'list' }, {});
        return response;
    }
    async findUserById(id) {
        const response = await this.post({ service: 'user', method: 'findUserById', id }, {});
        return response;
    }
}

export default ApiUser;
